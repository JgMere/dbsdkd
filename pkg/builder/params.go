package builder

import (
	"gitee.com/JgMere/dbsdkd/pbfiles"
	"google.golang.org/protobuf/types/known/structpb"
)

//程序员在囧途(www.jtthink.com)出品 咨询群：98514334

//参数构建器
type ParamBuilder struct {
	param map[string]interface{}
}

func NewParamBuilder() *ParamBuilder {
	return &ParamBuilder{param: make(map[string]interface{})}
}
func (this *ParamBuilder) Add(name string, value interface{}) *ParamBuilder {
	this.param[name] = value
	return this
}
func (this *ParamBuilder) Build() *pbfiles.SimpleParams {
	paramStruct, _ := structpb.NewStruct(this.param)
	return &pbfiles.SimpleParams{
		Params: paramStruct,
	}
}

//程序员在囧途(www.jtthink.com)出品 咨询群：98514334
