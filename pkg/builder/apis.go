package builder

import (
	"context"
	"gitee.com/JgMere/dbsdkd/pbfiles"
	"gitee.com/JgMere/dbsdkd/pkg/helpers"
	"github.com/mitchellh/mapstructure"
)

//程序员在囧途(www.jtthink.com)出品 咨询群：98514334

const (
	APITYPE_QUERY = iota
	APITYPE_EXEC
)

type ApiBuilder struct {
	name    string //api 名称
	apitype int
}

func NewApiBuilder(name string, apitype int) *ApiBuilder {
	return &ApiBuilder{name: name, apitype: apitype}
}

//普通执行， 不是事务,事务在txapi
func (this *ApiBuilder) Invoke(ctx context.Context, paramBuilder *ParamBuilder,
	client pbfiles.DBServiceClient, out interface{}) error {
	if this.apitype == APITYPE_QUERY { //查询
		req := &pbfiles.QueryRequest{Name: this.name, Params: paramBuilder.Build()}
		rsp, err := client.Query(ctx, req)
		if err != nil {
			return err
		}
		if out != nil { //如果 out 没有传值 不做转换
			return mapstructure.WeakDecode(helpers.PbstructsToMapList(rsp.GetResult()), out)
		}
		return nil

	} else { //增删改
		req := &pbfiles.ExecRequest{Name: this.name, Params: paramBuilder.Build()}
		rsp, err := client.Exec(ctx, req)
		if err != nil {
			return err
		}
		if out != nil { //如果 out 没有传值 不做转换
			var m map[string]interface{}
			if rsp.Select != nil {
				m = rsp.Select.AsMap()
				m["_RowsAffected"] = rsp.RowsAffected
			} else {
				m = map[string]interface{}{"_RowsAffected": rsp.RowsAffected}
			}
			return mapstructure.WeakDecode(m, out)
		}
		return nil

	}

}

//程序员在囧途(www.jtthink.com)出品 咨询群：98514334
