package main

import (
	"gitee.com/JgMere/dbsdkd/examples"
	"sync"
)

var wg = sync.WaitGroup{}

//程序员在囧途(www.jtthink.com)出品 咨询群：98514334
func main() {
	wg.Add(2)
	go func() {
		defer wg.Done()
		examples.TxStocktest(101, 2, 1)
	}()
	go func() {
		defer wg.Done()
		examples.TxStocktest(101, 3, 1)
	}()
	wg.Wait()

	//https://blog.csdn.net/lp2388163/article/details/80683383

}

//程序员在囧途(www.jtthink.com)出品 咨询群：98514334
